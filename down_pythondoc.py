#!/usr/bin/python
__metaclass__=type

import webbrowser
import urllib
import urlparse
import fileinput
import re
import os
import sys
import threading

class LibraryReference:
    """This class is solely to download Python Library Reference: https://docs.python.org/2/library/
       This class does not download external source of self.lr_main_url
    """
    def __init__(self, main_url=""):
        self._main_url=main_url
        self._domain, self._main_folder, self._path_under_main_folder = LibraryReference.getLocalMainFolder(main_url)
        #up=urlparse.urlparse(self._main_url)
        #self._domain=up.netloc                                          # domain name
        #self._main_folder="."+self._domain                              # create hiden main folder such as '.docs.python.org'
        #self._path_under_main_folder=up.path[up.path.index("/")+1:]     # remove '/' from url path, since it will make os.path.join not work properly

    @classmethod
    def getLocalMainFolder(self, main_url=""):
        "get local main folder"
        up=urlparse.urlparse(main_url)
        return (up.netloc, "."+up.netloc, up.path[up.path.index("/")+1:])     # remove '/' from url path, since it will make os.path.join not work properly

    def _getFiles(self, original_paths=[], download=False):
        """download or analyze the page
           if download set to False, _getFile will not download any files to local
           but just show full local path of files
        """
        local_paths=[]                                                  # return list of all downloaded file paths.
        original_paths=set(original_paths)                              # omit duplicated original paths appeared in html pages.

        #download pages to local
        for path in original_paths: 
            if not re.search("https?:\/\/", path):
                downloadable_url=urlparse.urljoin(self._main_url, path)     # since it's internal src, we must add domain in front of.
                local_path=os.path.join(self._main_folder,self._path_under_main_folder,path)
                local_paths.append(local_path)
            else:
                #external src, we are going to put into main domain, ie. 
                #https://hg.python.org/cpython/file/2.7/Lib/fileinput.py -> .docs.python.org/.hg.python.org/cpython/file/2.7/Lib/fileinput.py
                downloadable_url=path                                       
                up=urlparse.urlparse(path)
                domain=up.netloc
                ex_path=up.path[up.path.index("/")+1:]
                local_path=os.path.join(self._main_folder,domain, ex_path)
                local_paths.append(local_path)
            try:
                os.makedirs(os.path.dirname(local_path))
            except Exception as e:
                #sys.stderr.write(str(e))
                pass

            if download:
                print "download from {}\tto\t{}".format(downloadable_url, local_path)
                try:
                    urllib.urlretrieve(downloadable_url, local_path)
                except Exception as  e:
                    sys.stderr.write(str(e))
                    pass

        return local_paths

    def _analyzeSingleRemotePage(self, page=""):
        "return original paths, everything"
        page_urls=[]
        downloadable_url=urlparse.urljoin(self._main_url, page)
        lr=urllib.urlopen(downloadable_url)
        self._content=lr.read()
        #include itself
        page_urls.append(page)
        page_urls.extend(self._zzz(self._content))                             # attentioin! you can not use append, because return value is list
        return list(set(page_urls))

    def _zzz(self, content):
        "work on content"
        page_urls=[]
        css_urls=re.findall("link.*?href=\"(.*?\.css)\"", content, re.I)
        png_urls=re.findall("(?:img|link).*?(?:src|href)=\"(.*?\.png)\"", content, re.I)
        js_urls=re.findall("script.*?src=\"(.*?\.js)\"", content, re.I)
        html_urls=re.findall("href=\"([^\"]*?\.html)\"", content, re.I)
        py_urls=re.findall("href=\"([^\"]*?\.py)\"", content, re.I)
        page_urls.extend(set(css_urls))
        page_urls.extend(set(png_urls))
        page_urls.extend(set(js_urls))
        page_urls.extend(set(html_urls))
        page_urls.extend(set(py_urls))
        return page_urls
    
    def _analyzeLocal(self, local_paths=[]):
        """only get html pages from local_paths
           a little complicated while analyze local html files, we must consider original html page path, get dirname of original page, add urls from it and see if 
           we can find the page
        """
        total_urls=[]
        local_html_pages=[ x for x in local_paths if re.search(".html$", x) ]
        for line in fileinput.input(local_html_pages, mode="r"):
            urls=set(self._zzz(line))
            for url in urls:
                if not os.path.isfile(os.path.join(os.path.dirname(fileinput.filename()), url)):         # file already exists, we must omit it
                    total_urls.append(url)
        return list(set(total_urls))

    def start(self, mode="main"):
        "this func is main method"
        #analyze main page
        main_urls=[]
        print "analyze main page..."
        main_urls=self._analyzeSingleRemotePage("index.html")

        #download all pages,files apeared in the main page.
        local_paths=self._getFiles(main_urls, True)
        other_urls=self._analyzeLocal(local_paths)
        if mode == 'all':
            self._getFiles(set(other_urls), True)

        #analyze local pages.
        #print self._total_urls
        #self._work_on_pages(self._total_urls,"download"):
