#!/usr/bin/env bash
#
# version: 1.0
# Author: Nathan Huang/黄楠
#
#

uname="uname -s"

if [ "$uname" == "Linux" ]; then
    LN=`which ln`
    down=0
    paths=`echo $PATH | tr ":" "\n" | grep -i home`

    for path in ${paths}; do

        if [ -d ${path} ]; then

            $LN -s ./mann.py  ${path}/mann 2>/dev/null
	    if [ "$?" == 0 ]; then
	        echo make a softlink of mann.py to "$path"/mann
	    fi 
            down=1
	    exit 0
        fi
    done

    if [ -z "$down" ]; then
        echo no proper path for intallation.
    fi
fi
