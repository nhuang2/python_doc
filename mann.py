#!/usr/bin/python
__metaclass__=type

import os
import sys
import getopt
import webbrowser as wbr
from down_pythondoc import LibraryReference

class SearchEngine:
    """This class is for search keyword from CDL to get relative file to open"""
    def __init__(self, url, keyword):
        self._url=url
        self._keyword=keyword
        self._lynx=wbr.get('lynx')
        self._domain, self._main_folder, self._path_under_main_folder = LibraryReference.getLocalMainFolder(url)
        self._main_folder=os.path.join(self._main_folder, self._path_under_main_folder)
        if not os.path.isdir(self._main_folder):
            print "{} does not exits!, please download first!".format(self._main_folder)
            sys.exit(2)

    def _isman(self):
        "if keyword is man, open index.html"
        if self._keyword == 'man':
            print self._main_folder  
            
    def _searchFileName(self):
        "return result or False"
        for root, dirs, files in os.walk(self._main_folder):
            if self._main_folder == root:
                for ff in files:
                    ky=self._keyword.lower()
                    fs=ff.lower()
                    if self._keyword == fs[:fs.index(".html")]:
                        self._lynx.open(os.path.join(self._main_folder,ff))
                        return True
                # if keyword is not analyzable, open index.html
                for ff in files:
                    if ff.lower() == "index.html":
                        self._lynx.open(os.path.join(self._main_folder,ff))
                        return True
        return False

    def _searchTitle(self):
        "return result or -1"
        return -1

    def start(self):
        "searching start from here"
        if self._searchFileName():
            print "keyword is not correct!"


def help():
    print """
        -h [--help] for help
        -d [--download] main|all
    """
    sys.exit(2)

if __name__ == '__main__':
    keyword=""                          #keyword for searching documentation page
    mode=""
    url="https://docs.python.org/2/library/"

    opts, args = getopt.getopt(sys.argv[1:], shortopts="hd:", longopts=["--help", "--download"])
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            help()
        elif opt in ('-d', '--download'):
            mode=arg
            

        lr = LibraryReference(url)
    if mode == 'all':
        lr.start(mode)
        sys.exit(0)
    elif mode == 'main':
        lr.start()
        sys.exit(0)
    elif not mode:
        # only first arguments will be obtained.
        if not args:
            help()
        else:
            keyword = args[0]
            se = SearchEngine(url, keyword)
            se.start()
    else:
        help()
